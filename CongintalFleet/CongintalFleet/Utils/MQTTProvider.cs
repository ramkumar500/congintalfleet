﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CongintalFleet.Helpers
{
    public class MQTTProvider
    {
        private IMqttClient _mqttClient;

        public string ServerURL { get; set; }
        public string ClientId { get; set; }
        public int ServerPort { get; set; }

        public delegate void MessageReceivedHandler(string messageBody);
        public event MessageReceivedHandler MessageReceived;

        public async Task<bool> ConnectToServer()
        {
            bool connectionResult = false;
            MqttClientTlsOptions tlsOptions = new MqttClientTlsOptions
            {
                UseTls = false,
                IgnoreCertificateChainErrors = true,
                IgnoreCertificateRevocationErrors = true,
                AllowUntrustedCertificates = true
            };
            MqttClientOptions clientOptions = new MqttClientOptions { ClientId = this.ClientId };
            clientOptions.ChannelOptions = new MqttClientTcpOptions
            {
                Server = ServerURL,
                Port = ServerPort,
                TlsOptions = tlsOptions
            };

            try
            {
                if (_mqttClient != null)
                {
                    await _mqttClient.DisconnectAsync();
                    _mqttClient.ApplicationMessageReceived -= OnApplicationMessageReceived;
                }

                var factory = new MqttFactory();
                _mqttClient = factory.CreateMqttClient();
                _mqttClient.ApplicationMessageReceived += OnApplicationMessageReceived;
                MessageReceived("Connecting to Server ...");
                await _mqttClient.ConnectAsync(clientOptions);

                bool isConnected = _mqttClient.IsConnected;
                if (isConnected == true)
                {
                    await PublishMessage();
                    await Task.Delay(2000);
                    await SubscribeMessage();
                }
            }
            catch (Exception exception)
            {
                //Trace.Text += exception + Environment.NewLine;
            }
            finally
            {
                //await _mqttClient.DisconnectAsync();
            }
            return connectionResult;
        }

        public async Task DisConnect()
        {
            if (_mqttClient.IsConnected == true)
            {
                try
                {
                    await _mqttClient.DisconnectAsync();
                }
                catch
                {

                }
            }
        }

        private async Task PublishMessage()
        {
            if (_mqttClient == null)
            {
                return;
            }

            try
            {
                var qos = MqttQualityOfServiceLevel.AtMostOnce;

                var payload = Encoding.UTF8.GetBytes("{'Latitude': -12.45,'Longitude': 24.4} ");

                var message = new MqttApplicationMessageBuilder()
                    .WithTopic(@"user\45703b79-c422-4c48-bed4-f5be935f9922")
                    .WithPayload(payload)
                    .WithQualityOfServiceLevel(qos)
                    .WithRetainFlag(false)
                    .Build();
                MessageReceived("Publishing ...");
                await _mqttClient.PublishAsync(message);
            }
            catch (Exception exception)
            {
                //Trace.Text += exception + Environment.NewLine;
            }
        }

        private async Task SubscribeMessage()
        {
            if (_mqttClient == null)
            {
                return;
            }

            try
            {
                var qos = MqttQualityOfServiceLevel.AtMostOnce;

                MessageReceived("Subscribing ...");
                await _mqttClient.SubscribeAsync(new TopicFilter("#", qos));
            }
            catch (Exception exception)
            {
                //Trace.Text += exception + Environment.NewLine;
            }
        }


        private async void OnApplicationMessageReceived(object sender, MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            string message = $"Timestamp: {DateTime.Now:O} | Topic: {eventArgs.ApplicationMessage.Topic} | Payload: {Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload)} | QoS: {eventArgs.ApplicationMessage.QualityOfServiceLevel}";
            MessageReceived(message);
        }
    }
}
