﻿using CongintalFleet.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CongintalFleet
{
	public partial class MainPage : ContentPage
	{
        private MQTTProvider provider = null;
        public MainPage()
		{
			InitializeComponent();
		}

        protected async void buttonCheck_Clicked(object sender,EventArgs e)
        {
            try
            {
                if (provider == null)
                {
                    provider = new MQTTProvider();
                }
                else
                {
                    await provider.DisConnect();
                    return;
                }
                provider.ServerURL = "fms.conigitalcloud.com";
                provider.ServerPort = 1883;
                provider.ClientId = "45703b79-c422-4c48-bed4-f5be935f9922";

                provider.MessageReceived += Provider_MessageReceived;
                bool connectionResult = await provider.ConnectToServer();
            }
            catch
            {

            }
        }

        private void Provider_MessageReceived(string messageBody)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                labelItem.Text = messageBody;
            });
        }
    }
}
